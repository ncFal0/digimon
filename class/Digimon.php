<?php
class Digimon {
	public const NIVELES = ["joven","adulto","perfecto"];
	public const TIPOS = ["libre","vacuna","virus","datos"];
	public const ATRIBUTOS = ["neutral","planta","agua","fuego","viento","tierra","electrico","luz","oscuridad"];

	private string $nombre;
	private int $ataque;
	private int $defensa;
	private string $nivel;
	private array $evolucion;
	private string $tipo;
	private string $atributo;

	public function __construct(string $nom, int $atq, int $def, int $niv = 0, int $tip = 0, int $atr = 0) {
		$this->nombre = $nom;
		$this->ataque = $atq;
		$this->defensa = $def;
		$this->nivel = self::NIVELES[$niv];
		$this->evolucion = [];
		$this->tipo = self::TIPOS[$tip];
		$this->atributo = self::ATRIBUTOS[$atr];
	}

	public function get_nombre(): string {
		return $this->nombre;
	}

	public function get_ataque(): int {
		return $this->ataque;
	}

	public function get_defensa(): int {
		return $this->defensa;
	}

	public function get_nivel(): string {
		return $this->nivel;
	}

	public function get_nivel_numero(): int {
		return array_flip(self::NIVELES)[$this->nivel];
	}

	public function get_evolucion(): array {
		return $this->evolucion;
	}

	public function get_tipo(): string {
		return $this->tipo;
	}

	public function get_atributo(): string {
		return $this->atributo;
	}

	public function set_nombre(string $nombre) {
		$this->nombre = $nombre;
	}

	public function set_ataque(int $ataque) {
		$this->ataque = $ataque;
	}

	public function set_defensa(int $defensa) {
		$this->defensa = $defensa;
	}

	public function set_nivel(int $nivel) {
		$this->nivel = self::NIVELES[$nivel];
	}

	public function set_evolucion(string $evolucion) {
		$this->evolucion[] = $evolucion;
	}

	public function set_tipo(int $tipo) {
		$this->tipo = self::TIPOS[$tipo];
	}

	public function set_atributo(int $atributo) {
		$this->atributo = self::ATRIBUTOS[$atributo];
	}

	public function muestra_evoluciones(): string {
		$cadena = "";
		foreach($this->evolucion as $indice => $evolucion) {
			$cadena .= $evolucion;
			$cadena .= ($indice == count($this->evolucion) - 1) ? "" : ", ";
		}
		return $cadena;
	}
}
?>
