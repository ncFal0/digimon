<?php
class Usuario {
	private string $nick;
	private string $pass;

	public function __construct(string $nick, string $pass)
	{
		$this->nick = $nick;
		$this->pass = $pass;
	}

	public function get_nick(): string {
		return $this->nick;
	}

	public function get_pass(): string {
		return $this->pass;
	}

	public function set_nick(string $nick) {
		$this->nick = $nick;
	}

	public function set_pass(string $pass) {
		$this->pass = $pass;
	}
}
?>
