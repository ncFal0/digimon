<?php
require_once 'class/Digimon.php';
require_once 'class/Usuario.php';

function array_cadena(array $a): string {
	return urlencode(serialize($a));
}

function cadena_array(string $s): array {
	$a = [];
	if($s != '') {
		$a = unserialize(urldecode(stripslashes($s)));
	}
	return $a;
}

function guardar(array $datos, string $nombre) {
	$archivo = fopen($nombre, 'w');
	fwrite($archivo, serialize($datos));
	fclose($archivo);
}

function cargar(string $nombre): array {
	$archivo = fopen($nombre, 'r');
	$datos = unserialize(fgets($archivo));
	fclose($archivo);

	return $datos;
}

function volver_intentar(string $codigo) {
	$mensajes = [
		"El nombre de usuario no está registrado",
		"La contraseña es incorrecta"
	];

	echo "<form action=\"login.php\" method=\"POST\">";
	echo "<p>{$mensajes[$codigo]}</p>";
	echo "<input type=\"submit\" value=\"Volver a intentar\">";
	echo "</form>";
}

/*
function adelante(string $codigo, string $nick) {
	$modo = [
		"admin",
		"usuario"
	];

?>
	<form action='<?=$modo[$codigo] . '/' . $modo[$codigo] . '.php'?>' method="POST">
		<p>Adelante <?=$nick?></p>
		<input type="submit" name="adelante" value="Continuar">
	</form>
<?php
}
 */

function muestra_options(array $datos) {
	foreach($datos as $indice => $valor) {
		echo "<option value='{$indice}'>{$valor}</option>";
	}
}

function muestra_volver(string $pagina) {
	?>
	<form action='<?=$pagina?>' method="POST">
		<input type="submit" name="volver" value="Volver">
	</form>
	<?php
}

function in_digimones(string $nombre, array $digimones): bool {
	foreach($digimones as $nivel) {
		if(array_key_exists($nombre, $nivel)) {
			return true;
		}
	}
	return false;
}

function otorga_digimon(array $digimones, array $coleccion): ?Digimon {
	$num_rand = 0;
	$nombres = array_keys($digimones);

	if(!(count($digimones) == count($coleccion))) {
		do {
			$num_rand = rand(0, count($digimones) - 1);
		} while(array_key_exists($nombres[$num_rand], $coleccion));

		return $digimones[$nombres[$num_rand]];
	}

	return null;
}

function otorga_iniciales(array $digimones): array {
	$coleccion = [
		[],
		[],
		[]
	];

	for($i = 0; $i < 3; $i++) {
		$digimon = otorga_digimon($digimones[0], $coleccion[0]);
		$coleccion[$digimon->get_nivel_numero()][$digimon->get_nombre()] = $digimon;
	}
	return $coleccion;
}

function equipo_inicial(array $coleccion): array {
	$equipo = [];

	foreach($coleccion[0] as $inicial) {
		$equipo[] = $inicial;
	}

	return $equipo;
}

/*
function digievolucion(Digimon $digimon, array $coleccion, array $digimones): ?Digimon {
	$evolucion = $digimones[$digimon->get_nivel_numero() + 1][$digimon->get_evolucion()];

	if(!in_digimones($evolucion->get_nombre(), $coleccion)) {
		return $evolucion;
	}

	return null;
}
 */

function ver_imagen(string $nombre, int $modo, string $ruta_directorio): string {
	$modos = ["normal","victoria","derrota"];

	$ruta_imagen = "{$ruta_directorio}/{$nombre}/{$modos[$modo]}.jpg";

	return "<img src='{$ruta_imagen}' style='width: 160px; height: 160px;'>";
}
