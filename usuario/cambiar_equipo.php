<?php
require_once '../funciones.php';
require_once '../class/Digimon.php';

function muestra_options_cambiar(array $coleccion, array $equipo) {
	foreach($coleccion as $numero => $nivel) {
		foreach($nivel as $indice => $digimon) {
			if(!in_array($digimon, $equipo)) {
				echo "<option value='{$indice}:{$numero}'>{$indice}</option>";
			}
		}
	}
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}
if(!isset($_POST['nombre'], $_POST['posicion'])) {
	header('location:usuario.php');
	exit();
}

$nombre = $_POST['nombre'];
$posicion = $_POST['posicion'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cambiar Digimon</title>
</head>
<body>
	<form action="organizar_equipo.php" method="POST">
		<label for="cambiar">Cambiar <?=$nombre?> por: </label>
		<select name="cambiar" id="cambiar" required>
			<?=muestra_options_cambiar($_SESSION['coleccion'], $_SESSION['equipo'])?>
		</select>
		<input type="submit" value="Hecho">
		<input type="hidden" name="nombre" value='<?=$nombre?>'>
		<input type="hidden" name="posicion" value='<?=$posicion?>'>
	</form>
	<?=muestra_volver('organizar_equipo.php')?>
</body>
</html>
