<?php
require_once '../class/Digimon.php';
require_once '../funciones.php';

function mostrar_digievolucionar(Digimon $digimon) {
	?>
	<form action="digievolucionar.php" method="POST">
		<input type="submit" value="Digievolucionar">
		<input type="hidden" name="nombre" value='<?=$digimon->get_nombre()?>'>
		<input type="hidden" name="nivel" value='<?=$digimon->get_nivel_numero()?>'>
	</form>
	<?php
}

function ver_digimones(array $digimones) {
	echo "<table border='1'>";
	echo "<tr>";
	echo "<td></td>";
	echo "<th>Nombre</th>";
	echo "<th>Ataque</th>";
	echo "<th>Defensa</th>";
	echo "<th>Nivel</th>";
	echo "<th>Evoluciones posibles</th>";
	echo "<th>Tipo</th>";
	echo "<th>Atributo</th>";

	foreach($digimones as $numero => $nivel) {
		foreach($nivel as $nombre => $digimon) {
			echo "<tr>";
			?>
			<td><?=ver_imagen($digimon->get_nombre(),0,'../data/digimones')?></td>
			<td><?=$digimon->get_nombre()?></td>
			<td><?=$digimon->get_ataque()?></td>
			<td><?=$digimon->get_defensa()?></td>
			<td><?=$digimon->get_nivel()?></td>
			<td><?=$digimon->muestra_evoluciones()?></td>
			<td><?=$digimon->get_tipo()?></td>
			<td><?=$digimon->get_atributo()?></td>
			<td><?=($numero < 2) ? mostrar_digievolucionar($digimon) : "Nivel máximo"?></td>
			<?php
			echo "</tr>";
		}
	}
	echo "</tr>";
	echo "</table>";
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['evo'], $_POST['nombre'], $_POST['nivel'])) {
	$nombre = $_POST['nombre'];
	$nivel = $_POST['nivel'];
	$evo = $_POST['evo'];

	$cadena_resultado = "<p style='color: red;'>No puedes tener Digimones repetidos</p>";
	if(!in_digimones($evo, $_SESSION['coleccion'])) {
		$digimon = $_SESSION['digimones'][$nivel][$nombre];
		$digievo = $_SESSION['digimones'][$nivel + 1][$evo];
	
		unset($_SESSION['coleccion'][$nivel][$nombre]);
		$_SESSION['coleccion'][$nivel + 1][$evo] = $digievo;
		if(in_array($digimon, $_SESSION['equipo'])) {
			$posicion = array_search($digimon, $_SESSION['equipo']);
			$_SESSION['equipo'][$posicion] = $digievo;

			guardar($_SESSION['equipo'], "../data/usuarios/{$_SESSION['usuario']}/equipo");
		}
		guardar($_SESSION['coleccion'], "../data/usuarios/{$_SESSION['usuario']}/coleccion");

		$_SESSION['partidas']['tokens']--;
		guardar($_SESSION['partidas'], "../data/usuarios/{$_SESSION['usuario']}/partidas");

		$cadena_resultado = "<p style='color: green;'>Tu {$nombre} ha digievolucionado en {$evo}!</p>";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver mis Digimon</title>
</head>
<body>
	<?=$cadena_resultado?>

	<?=ver_digimones($_SESSION['coleccion'])?>

	<?=muestra_volver('usuario.php')?>
</body>
</html>
