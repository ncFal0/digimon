<?php
require_once '../class/Digimon.php';
require_once '../funciones.php';

function mostrar_cambiar(Digimon $digimon, int $posicion) {
	?>
	<form action="cambiar_equipo.php" method="POST">
		<input type="submit" value="Cambiar">
		<input type="hidden" name="nombre" value='<?=$digimon->get_nombre()?>'>
		<input type="hidden" name="posicion" value='<?=$posicion?>'> 
	</form>
	<?php
}

function ver_digimones(array $digimones) {
	echo "<table border='1'>";
	echo "<tr>";
	echo "<td></td>";
	echo "<th>Nombre</th>";
	echo "<th>Ataque</th>";
	echo "<th>Defensa</th>";
	echo "<th>Nivel</th>";
	echo "<th>Tipo</th>";
	echo "<th>Atributo</th>";

	foreach($digimones as $posicion => $digimon) {
		echo "<tr>";
		?>
		<td><?=ver_imagen($digimon->get_nombre(),0,'../data/digimones')?></td>
		<td><?=$digimon->get_nombre()?></td>
		<td><?=$digimon->get_ataque()?></td>
		<td><?=$digimon->get_defensa()?></td>
		<td><?=$digimon->get_nivel()?></td>
		<td><?=$digimon->get_tipo()?></td>
		<td><?=$digimon->get_atributo()?></td>
		<td><?=mostrar_cambiar($digimon, $posicion)?></td>
		<?php
		echo "</tr>";
	}
	echo "</tr>";
	echo "</table>";
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['cambiar'], $_POST['nombre'], $_POST['posicion'])) {
	$nombre_antiguo = $_POST['nombre'];

	$nombre = explode(':', $_POST['cambiar'])[0];
	$nivel = explode(':', $_POST['cambiar'])[1];

	$posicion = $_POST['posicion'];

	$_SESSION['equipo'][$posicion] = $_SESSION['coleccion'][$nivel][$nombre];

	guardar($_SESSION['equipo'], "../data/usuarios/{$_SESSION['usuario']}/equipo");

	$cadena_resultado = "<p>Has cambiado a {$nombre_antiguo} por {$nombre}</p>";
}
/*
if(isset($_POST['evo'], $_POST['nombre'], $_POST['nivel'])) {
	$nombre = $_POST['nombre'];
	$nivel = $_POST['nivel'];
	$evo = $_POST['evo'];

	$cadena_resultado = "<p style='color: red;'>No puedes tener Digimones repetidos</p>";
	if(!in_digimones($evo, $_SESSION['coleccion'])) {
		$digimon = $_SESSION['digimones'][$nivel][$nombre];
		$digievo = $_SESSION['digimones'][$nivel + 1][$evo];
	
		unset($_SESSION['coleccion'][$nivel][$nombre]);
		$_SESSION['coleccion'][$nivel + 1][$evo] = $digievo;
		if(in_array($digimon, $_SESSION['equipo'])) {
			$posicion = array_search($digimon, $_SESSION['equipo']);
			$_SESSION['equipo'][$posicion] = $digievo;

			guardar($_SESSION['equipo'], "../data/usuarios/{$_SESSION['usuario']}/equipo");
		}
		guardar($_SESSION['coleccion'], "../data/usuarios/{$_SESSION['usuario']}/coleccion");

		$cadena_resultado = "<p style='color: green;'>Tu {$nombre} ha digievolucionado en {$evo}!</p>";
	}
}
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Organizar equipo</title>
</head>
<body>
	<?=$cadena_resultado?>

	<?=ver_digimones($_SESSION['equipo'])?>

	<?=muestra_volver('usuario.php')?>
</body>
</html>
