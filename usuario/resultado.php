<?php
require_once '../funciones.php';
require_once '../class/Digimon.php';

function calcula_mult(array $tipos, array $multiplicador): float {
	return $multiplicador[$tipos[0]][$tipos[1]];
}

function calcula_afin(array $atributos, array $afinidad): float {
	if(isset($afinidad[$atributos[0]])) {
		if($afinidad[$atributos[0]] == $atributos[1]) {
			return 1.5;
		}
	}
	return 1;
}

function calcular_fuerza(Digimon $digimon): float {
	return $digimon->get_ataque() + $digimon->get_defensa();
}

function combate(Digimon $propio, Digimon $rival, array $mult, array $afin): bool {
	$fuerza_propio = calcular_fuerza($propio);
	$fuerza_propio *= calcula_mult([$propio->get_tipo(), $rival->get_tipo()], $mult);
	$fuerza_propio *= calcula_afin([$propio->get_atributo(), $rival->get_atributo()], $afin);
	$fuerza_propio += rand(1,50);

	$fuerza_rival = calcular_fuerza($rival);
	$fuerza_rival *= calcula_mult([$rival->get_tipo(), $propio->get_tipo()], $mult);
	$fuerza_rival *= calcula_afin([$rival->get_atributo(), $propio->get_atributo()], $afin);
	$fuerza_rival += rand(1,50);

	if($fuerza_propio == $fuerza_rival) {
		return (bool) rand(0,1);
	}
	return ($fuerza_propio > $fuerza_rival) ? true : false;
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}

if(!isset($_POST['rival'])) {
	header('location:jugar_partida.php');
	exit();
}

$rival = $_POST['rival'];

$equipo = $_SESSION['equipo'];
$equipo_rival = cargar("../data/usuarios/{$rival}/equipo");

if(!isset($_SESSION['mult'], $_SESSION['afin'])) {
	$_SESSION['mult'] = cargar('../text/multiplicador');
	$_SESSION['afin'] = cargar('../text/afinidad');
}

$cadena_resultado = "";

$combinaciones = [
	[0,1,2],
	[0,2,1],
	[1,0,2],
	[1,2,0],
	[2,0,1],
	[2,1,0]
];

$num_rand_propio = rand(0,5);
$num_rand_rival = rand(0,5);

$combates_ganados = 0;
for($i = 0; $i < 3; $i++) {
	$p = $combinaciones[$num_rand_propio][$i];
	$r = $combinaciones[$num_rand_rival][$i];

	$cadena_resultado .= "<tr>";
	if(combate($equipo[$p], $equipo_rival[$r], $_SESSION['mult'], $_SESSION['afin'])) {
		$combates_ganados++;
		
		$cadena_resultado .= "<td>" . ver_imagen($equipo[$p]->get_nombre(),1,'../data/digimones') . "</td>";
		$cadena_resultado .= "<td>Tu " . $equipo[$p]->get_nombre() . 
			" ha ganado a " . $equipo_rival[$r]->get_nombre() . "</td>";
		$cadena_resultado .= "<td>" . ver_imagen($equipo_rival[$r]->get_nombre(),2,'../data/digimones') . "</td>";
	}
	else {
		$cadena_resultado .= "<td>" . ver_imagen($equipo[$p]->get_nombre(),2,'../data/digimones') . "</td>";
		$cadena_resultado .= "<td>Tu " . $equipo[$p]->get_nombre() .
			" ha perdido contra " . $equipo_rival[$r]->get_nombre() . "</td>";
		$cadena_resultado .= "<td>" . ver_imagen($equipo_rival[$r]->get_nombre(),1,'../data/digimones') . "</td>";
	}
	$cadena_resultado .= "</tr>";
}

$_SESSION['partidas']['jugadas']++;

if($combates_ganados >= 2) {
	$_SESSION['partidas']['ganadas']++;

	if($_SESSION['partidas']['ganadas'] % 10 == 0) {
		$digimon_otorgado = otorga_digimon($_SESSION['digimones'][0], $_SESSION['coleccion'][0]);
		$_SESSION['coleccion'][0][$digimon_otorgado->get_nombre()] = $digimon_otorgado;
		guardar($_SESSION['coleccion'], "../data/usuarios/{$_SESSION['usuario']}/coleccion");
	}

	$cadena_resultado .= "<tr><td><b>HAS GANADO</b></td></tr>";
}
else {
	$cadena_resultado .= "<tr><td><b>HAS PERDIDO</b></td></tr>";
}

if($_SESSION['partidas']['jugadas'] % 10 == 0) {
	$_SESSION['partidas']['tokens']++;
}

guardar($_SESSION['partidas'], "../data/usuarios/{$_SESSION['usuario']}/partidas");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Resultado</title>
</head>
<body>
	<table border="1">
		<?=$cadena_resultado?>
	</table>
	<?=muestra_volver('usuario.php')?>
</body>
</html>
