<?php
require_once '../class/Usuario.php';
require_once '../class/Digimon.php';
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}

$_SESSION['digimones'] = cargar('../text/digimones');
$_SESSION['coleccion'] = cargar('../data/usuarios/' . $_SESSION['usuario'] . '/coleccion');
$_SESSION['equipo'] = cargar('../data/usuarios/' . $_SESSION['usuario'] . '/equipo');
$_SESSION['partidas'] = cargar('../data/usuarios/' . $_SESSION['usuario'] . '/partidas');
?>
<form action="ver_mis_digimon.php" method="POST">
	<input type="submit" value="Ver mis Digimones">
</form>
<form action="organizar_equipo.php" method="POST">
	<input type="submit" value="Organizar equipo">
</form>
<form action="jugar_partida.php" method="POST">
	<input type="submit" value="Jugar partida">
</form>
<form action="../comprueba_usuario.php" method="POST">
	<input type="submit" name="cerrar_sesion" value="Cerrar Sesión">
</form>
<p>Partidas jugadas: <?=$_SESSION['partidas']['jugadas']?></p>
<p>Partidas ganadas: <?=$_SESSION['partidas']['ganadas']?></p>
<p>Tokens de evolución: <?=$_SESSION['partidas']['tokens']?></p>
