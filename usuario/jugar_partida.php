<?php
require_once '../funciones.php';
require_once '../class/Usuario.php';

function muestra_options_rival(array $usuarios, string $jugador) {
	foreach($usuarios as $usuario) {
		$nombre = $usuario->get_nick();
		if($nombre != 'admin' && $nombre != $jugador) {
			echo "<option value='{$nombre}'>{$nombre}</option>";
		}
	}
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Jugar partida</title>
</head>
<body>
	<form action="resultado.php" method="POST">
		<label for="rival">Elige un rival: </label>
		<select name="rival" id="rival">
			<?=muestra_options_rival($_SESSION['usuarios'], $_SESSION['usuario'])?>
		</select>
		<input type="submit" value="Jugar">
	</form>
	<?=muestra_volver('usuario.php')?>
</body>
</html>
