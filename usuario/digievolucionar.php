<?php
require_once '../funciones.php';
require_once '../class/Digimon.php';

function muestra_options_digievo(Digimon $digimon) {
	foreach($digimon->get_evolucion() as $evolucion) {
		echo "<option value='{$evolucion}'>{$evolucion}</option>";
	}
}

session_start();

if(!isset($_SESSION['usuario'])) {
	header('location:../login.php');
	exit();
}
if(!isset($_POST['nombre'], $_POST['nivel'])) {
	header('location:usuario.php');
	exit();
}

$nombre = $_POST['nombre'];
$nivel = $_POST['nivel'];

$digimon = $_SESSION['digimones'][$nivel][$nombre];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Digievolucionar</title>
</head>
<body>
	<p>Tokens: <?=$_SESSION['partidas']['tokens']?></p>
	<form action="ver_mis_digimon.php" method="POST">
		<label for="evo"><?=$nombre?> digievoluciona en: </label>
		<select name="evo" id="evo" required>
			<?=muestra_options_digievo($digimon)?>
		</select>
		<?=($_SESSION['partidas']['tokens'] > 0) ? '<input type="submit" value="Hecho">' : 'No tienes tokens'?>
		<input type="hidden" name="nombre" value='<?=$nombre?>'>
		<input type="hidden" name="nivel" value='<?=$nivel?>'>
	</form>
	<?=muestra_volver('ver_mis_digimon.php')?>
</body>
</html>
