<?php
require_once '../class/Digimon.php';
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['nom'],$_POST['atq'],$_POST['def'],$_POST['niv'],$_POST['tip'],$_POST['atr'])) {
	$nom = $_POST['nom'];
	$atq = $_POST['atq'];
	$def = $_POST['def'];
	$niv = $_POST['niv'];
	$tip = $_POST['tip'];
	$atr = $_POST['atr'];

	$cadena_resultado = "<p style='color: red;'>El nombre del digimon ya está registrado</p>";

	if(!in_digimones($nom, $_SESSION['digimones'])) {
		$digimon = new Digimon($nom, $atq, $def, $niv, $tip, $atr);
		$_SESSION['digimones'][$niv][$nom] = $digimon;
		guardar($_SESSION['digimones'], '../text/digimones');

		$directorio = '../data/digimones/';
		mkdir($directorio . $nom);

		copy($directorio . 'default/normal.jpg', $directorio . $nom . '/normal.jpg');
		copy($directorio . 'default/victoria.jpg', $directorio . $nom . '/victoria.jpg');
		copy($directorio . 'default/derrota.jpg', $directorio . $nom . '/derrota.jpg');

		$cadena_resultado = "<p style='color: green;'>Digimon registrado correctamente</p>";
	}
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Alta Digimon</title>
</head>
<body>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<div>
		<label for="nom">Nombre: </label>
		<input type="text" name="nom" id="nom" required>
		</div>

		<div>
		<label for="atq">Ataque: </label>
		<input type="number" name="atq" id="atq" min="0" required>
		</div>

		<div>
		<label for="def">Defensa: </label>
		<input type="number" name="def" id="def" min="0" required>
		</div>

		<div>
		<label for="niv">Nivel: </label>
		<select name="niv" id="niv" required>
		<?=muestra_options(Digimon::NIVELES)?>
		</select>
		</div>

		<div>
		<label for="tip">Tipo: </label>
		<select name="tip" id="tip" required>
		<?=muestra_options(Digimon::TIPOS)?>
		</select>
		</div>

		<div>
		<label for="atr">Atributo: </label>
		<select name="atr" id="atr" required>
		<?=muestra_options(Digimon::ATRIBUTOS)?>
		</select>
		</div>

		<input type="submit" name="confirmar" value="Dar de alta">
	</form>
	
	<?=muestra_volver('admin.php')?>

	<?=$cadena_resultado?>
</body>
</html>
