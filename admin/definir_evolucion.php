<?php
require_once '../class/Digimon.php';
require_once '../funciones.php';

function muestra_options_evo(array $datos) {
	foreach($datos as $indice => $valor) {
		echo "<option value='{$indice}'>{$indice}</option>";
	}
}

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

if(!isset($_POST['nombre'], $_POST['nivel'])) {
	header('location:ver_digimon.php');
	exit();
}

$cadena_resultado = "";

$nombre = $_POST['nombre'];
$nivel = $_POST['nivel'];

if(isset($_POST['evo'])) {
	if(in_array($_POST['evo'], $_SESSION['digimones'][$nivel][$nombre]->get_evolucion())) {
		$cadena_resultado = "<p style='color: red;'>{$nombre} ya podía evolucionar a {$_POST['evo']}</p>";
	}
	else {
		$_SESSION['digimones'][$nivel][$nombre]->set_evolucion($_POST['evo']);
		guardar($_SESSION['digimones'], '../text/digimones');

		$cadena_resultado = "<p style='color: green;'>Evolución definida con éxito</p>";
	}
}

/*
if(isset($_POST['evo'])) {
	$digimon = $_SESSION['digimon'];
	$_SESSION['digimones'][$digimon->get_nivel_numero()][$digimon->get_nombre()]->set_evolucion($_POST['evo']);
	guardar($_SESSION['digimones'], '../text/digimones');
}

if(!isset($_POST['nombre'], $_POST['nivel'])) {
	header('location:ver_digimon.php');
	exit();
}
else {
	$nombre = $_POST['nombre'];
	$nivel = $_POST['nivel'];

	$_SESSION['digimon'] = $_SESSION['digimones'][$nivel][$nombre];
}
 */
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Definir Evolucion</title>
</head>
<body>
	<p>Digimon: <?=$nombre?></p>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
		<label for="evo">Digievolución: </label>
		<select name="evo" id="evo" required>
			<?=muestra_options_evo($_SESSION['digimones'][$nivel + 1])?>	
		</select>
		<input type="submit" name="confirmar" value="confirmar">
		<input type="hidden" name="nombre" value='<?=$nombre?>'>
		<input type="hidden" name="nivel" value='<?=$nivel?>'>
	</form>

	<?=muestra_volver('ver_digimon.php')?>

	<?=$cadena_resultado?>
</body>
</html>
