<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

$_SESSION['digimones'] = cargar('../text/digimones');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
</head>
<body>
	<form action="alta_usuario.php" method="POST">
		<input type="submit" name="alta_usuario" value="Alta Usuario">
	</form>
	<form action="alta_digimon.php" method="POST">
		<input type="submit" name="alta_digimon" value="Alta Digimon">
	</form>
	<form action="ver_digimon.php" method="POST">
		<input type="submit" name="ver_digimon" value="Ver Digimones">
	</form>
	<form action="../comprueba_usuario.php" method="POST">
		<input type="submit" name="cerrar_sesion" value="Cerrar Sesión">
	</form>
</body>
</html>

<?php
echo "<pre>";
var_dump($_SESSION['usuarios']);
echo "</pre>";

echo "<pre>";
var_dump($_SESSION['digimones']);
echo "</pre>";
?>
