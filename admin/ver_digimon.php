<?php
require_once '../class/Digimon.php';
require_once '../funciones.php';

function mostrar_definir_evo(Digimon $digimon) {
	?>
	<form action="definir_evolucion.php" method="POST">
		<input type="submit" value="Definir evolución">
		<input type="hidden" name="nombre" value='<?=$digimon->get_nombre()?>'>
		<input type="hidden" name="nivel" value='<?=$digimon->get_nivel_numero()?>'>
	</form>
	<?php
}

function mostrar_cambio_imagen(Digimon $digimon) {
	?>
	<form action="imagen_digimon.php" method="POST">
		<input type="submit" value="Añadir imágenes">
		<input type="hidden" name="nombre" value='<?=$digimon->get_nombre()?>'>
	</form>
	<?php
}

function ver_digimones(array $digimones) {
	echo "<table border='1'>";
	echo "<tr>";
	echo "<th>Nombre</th>";
	echo "<th>Normal</th>";
	echo "<th>Victoria</th>";
	echo "<th>Derrota</th>";
	echo "<th>Ataque</th>";
	echo "<th>Defensa</th>";
	echo "<th>Nivel</th>";
	echo "<th>Evoluciones posibles</th>";
	echo "<th>Tipo</th>";
	echo "<th>Atributo</th>";

	foreach($digimones as $numero => $nivel) {
		foreach($nivel as $nombre => $digimon) {
			echo "<tr>";
			?>
			<td><?=$digimon->get_nombre()?></td>
			<td><?=ver_imagen($digimon->get_nombre(),0,'../data/digimones')?></td>
			<td><?=ver_imagen($digimon->get_nombre(),1,'../data/digimones')?></td>
			<td><?=ver_imagen($digimon->get_nombre(),2,'../data/digimones')?></td>
			<td><?=$digimon->get_ataque()?></td>
			<td><?=$digimon->get_defensa()?></td>
			<td><?=$digimon->get_nivel()?></td>
			<td><?=$digimon->muestra_evoluciones()?></td>
			<td><?=$digimon->get_tipo()?></td>
			<td><?=$digimon->get_atributo()?></td>
			<td><?=($numero < 2) ? mostrar_definir_evo($digimon) : "Nivel máximo"?></td>
			<td><?=mostrar_cambio_imagen($digimon)?></td>
			<?php
			echo "</tr>";
		}
	}
	echo "</tr>";
	echo "</table>";
}

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

/*
if(!isset($_SESSION['digimones'])) {
	$d = [
		[],
		[],
		[]
	];

	$d[0]["achan"] = new Digimon("achan",123,43,0);
	$d[0]["mamaguevo"] = new Digimon("mamaguevo",123,43,0);
	$d[0]["morena"] = new Digimon("morena",123,43,0);
	$d[0]["asa"] = new Digimon("asa",123,43,0);
	$d[1]["boochan"] = new Digimon("boochan",123,43,1);
	$d[1]["salamankesa"] = new Digimon("salamankesa",123,43,1);
	$d[2]["omega"] = new Digimon("omega",123,43,2);
	$d[2]["alfa"] = new Digimon("alfa",123,43,2);

	$_SESSION['digimones'] = $d;
}*/
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Digimon</title>
</head>
<body>
	<?=ver_digimones($_SESSION['digimones'])?>

	<?=muestra_volver('admin.php')?>
</body>
</html>
