<?php
require_once '../funciones.php';

function form_imagen(int $modo, string $nombre) {
	$modos = ["normal","victoria","derrota"];
	?>
	<form action="imagen_digimon.php" method="POST" enctype="multipart/form-data">
		<label for="imagen">Imagen <?=$modos[$modo]?>: </label>
		<input type="file" id="imagen" name='imagen' accept="image/jpeg">
		<input type="hidden" name="nombre" value='<?=$nombre?>'>
		<input type="hidden" name="modo" value='<?=$modos[$modo]?>'>
		<input type="submit" value="Subir imagen">
	</form>
	<?php
}

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

if(!isset($_POST['nombre'])) {
	header('location:ver_digimon.php');
	exit();
}

$cadena_resultado = "";
$nombre = $_POST['nombre'];

if(isset($_POST['modo'], $_FILES['imagen'])) {
	$modo = $_POST['modo'];
	$cadena_resultado = "<p style='color: red;'>Debes proporcionar un archivo de tipo jpeg (.jpg, .jpeg)</p>";
	if($_FILES['imagen']['type'] == "image/jpeg") {
		$temporal = $_FILES['imagen']['tmp_name'];
		$destino = "../data/digimones/{$nombre}/{$modo}.jpg";

		move_uploaded_file($temporal, $destino);

		$cadena_resultado = "<p style='color: green;'>Imagen subida con éxito</p>";
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Imagenes Subida</title>
</head>
<body>
	<p>Digimon: <?=$nombre?></p>
	<?php
	for($i = 0; $i < 3; $i++) {
		echo "<p>";
		form_imagen($i, $nombre);
		echo "</p>";
	}
	muestra_volver('ver_digimon.php');

	echo $cadena_resultado;
?>
</body>
</html>
