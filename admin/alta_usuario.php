<?php
require_once '../class/Usuario.php';
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['admin'])) {
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['nick'], $_POST['pass'])) {
	$nick = $_POST['nick'];
	$pass = $_POST['pass'];

	$cadena_resultado = "<p style='color: red;'>El nombre de usuario ya está registrado</p>";

	if(!array_key_exists($nick, $_SESSION['usuarios'])) {
		$usuario = new Usuario($nick, $pass);

		$_SESSION['usuarios'][$nick] = $usuario;

		guardar($_SESSION['usuarios'], '../text/usuarios');

		$coleccion = otorga_iniciales($_SESSION['digimones']);
		$equipo = equipo_inicial($coleccion);
		$partidas = ["jugadas" => 0, "ganadas" => 0, "tokens" => 0];

		$directorio = '../data/usuarios/';
		mkdir($directorio . $nick);

		guardar($coleccion, $directorio . $nick . '/coleccion');
		guardar($equipo, $directorio . $nick . '/equipo');
		guardar($partidas, $directorio . $nick . '/partidas');


		$cadena_resultado = "<p style='color: green;'>Usuario registrado correctamente</p>";
	}
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Alta Usuario</title>
</head>
<body>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<div>
		<label for="nick">Nombre de usuario: </label>
		<input type="text" name="nick" id="nick" required>
		</div>

		<div>
		<label for="pass">Contraseña: </label>
		<input type="password" name="pass" id="pass" required>
		</div>

		<input type="submit" name="confirmar" value="Dar de alta">
	</form>

	<?=muestra_volver('admin.php')?>

	<?=$cadena_resultado?>
</body>
</html>
