<?php
require_once 'funciones.php';
require_once 'class/Usuario.php';

session_start();

if(isset($_POST['cerrar_sesion'])) {
	$_SESSION = [];
	session_destroy();
}

if(!isset($_POST['nick'], $_POST['pass'])) {
	header('location:login.php');
	exit();
}
else {
	$usuarios = cargar('text/usuarios');

	$nick = $_POST['nick'];
	$pass = $_POST['pass'];

	if(!array_key_exists($nick, $usuarios)) {
		volver_intentar(0);
	}
	else {
		if($usuarios[$nick]->get_pass() != $pass) {
			volver_intentar(1);
		}
		else {
			$_SESSION['usuarios'] = $usuarios;

			if($nick == 'admin') {
				$_SESSION['admin'] = $nick;
				header('location:admin/admin.php');
				exit();
			}
			else {
				$_SESSION['usuario'] = $nick;
				header('location:usuario/usuario.php');
				exit();
			}
		}
	}
}
?>
