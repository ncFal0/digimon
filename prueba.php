<?php
require_once 'class/Digimon.php';
require_once 'class/Usuario.php';
require_once 'funciones.php';
/*
function guardar_usuarios(array $usuarios) {
	$archivo = fopen('usuarios', 'w');
	fwrite($archivo, serialize($usuarios));
	fclose($archivo);
}
function cargar_usuarios(): array {
	$archivo = fopen('usuarios', 'r');
	$usuarios = unserialize(fgets($archivo));
	fclose($archivo);

	return $usuarios;
}
function guardar_digimones(array $digimones) {
	$archivo = fopen('digimones', 'w');
	fwrite($archivo, serialize($digimones));
	fclose($archivo);
}
function cargar_digimones(): array {
	$archivo = fopen('digimones', 'r');
	$digimones = unserialize(fgets($archivo));
	fclose($archivo);

	return $digimones;
}

function guardar(array $datos, string $nombre) {
	$archivo = fopen($nombre, 'w');
	fwrite($archivo, serialize($datos));
	fclose($archivo);
}

function cargar(string $nombre): array {
	$archivo = fopen($nombre, 'r');
	$datos = unserialize(fgets($archivo));
	fclose($archivo);

	return $datos;
}

function array_cadena(array $a): string {
	return urlencode(serialize($a));
}

function cadena_array(string $s): array {
	$a = [];
	if($s != '') {
		$a = unserialize(urldecode(stripslashes($s)));
	}
	return $a;
}
$u1 = new Usuario('admin', '1234');

$usu = [
	$u1->get_nick() => $u1,
];

$d3 = new Digimon("fokon", 69420, 77777, 2, 2, 8);
$d2 = new Digimon("fok", 1337, 1312, 1, 2, 8);
$d1 = new Digimon("fokito", 420, 69, 0, 2, 8);
$d4 = new Digimon("achantamon", 420, 69, 0, 2, 8);
$d5 = new Digimon("payucomon", 420, 69, 0, 2, 8);
$d6 = new Digimon("awebomon", 420, 69, 0, 2, 8);

$d1->set_evolucion($d2->get_nombre());
$d2->set_evolucion($d3->get_nombre());

echo $d3->get_nivel_numero();

$nivel1 = [
	$d1->get_nombre() => $d1,
	$d4->get_nombre() => $d4,
	$d5->get_nombre() => $d5,
	$d6->get_nombre() => $d6
];
$nivel2 = [$d2->get_nombre() => $d2];
$nivel3 = [$d3->get_nombre() => $d3];

$a = [];
$a[] = $nivel1;
$a[] = $nivel2;
$a[] = $nivel3;

$n1 = $nivel1;
unset($n1['fokito']);
unset($n1['awebomon']);
echo "<pre>";
var_dump(otorga_digimon($a[0], $n1));
echo "</pre>";

echo "<pre>";
var_dump($a);
echo "</pre>";

$asa = cadena_array(array_cadena($a));

echo "<pre>";
var_dump($asa);
echo "</pre>";

$user = posix_getpwuid(posix_geteuid());
var_dump($user);

$ca = serialize($a);
echo $ca;

echo "<br>";
$user = posix_getpwuid(posix_geteuid());
var_dump($user);

echo "<br>";
echo substr(sprintf('%o', fileperms('.')), -4);
echo "<br>";
echo substr(sprintf('%o', fileperms('digimones')), -4);

$archivo_d = fopen('digimones', 'w');
fwrite($archivo_d, $ca);
fclose($archivo_d);

$archivo_d2 = fopen('digimones', 'r');
$ca2 = fgets($archivo_d2);
fclose($archivo_d2);

echo "<br>";
echo $ca2;

$a2 = unserialize($ca2);
echo "<pre>";
var_dump($a2);
echo "</pre>";
 

$b = cargar('text/digimones');
echo "<pre>";
var_dump($b);
echo "</pre>";
 */ 
//guardar([], 'text/digimones');
/*
$u1 = new Usuario('admin', '1234');
$usu = [
	$u1->get_nick() => $u1
];
guardar($usu, 'text/usuarios');
 */
/*
$dig = cargar('text/digimones');
unset($dig[0]['acho']);
guardar($dig, 'text/digimones');
 */
/*
$coleccion = cargar('data/usuarios/jugador1/coleccion');
$digimones = cargar('text/digimones');


$d0 = otorga_digimon($digimones[0], $coleccion[0]);
$d1 = otorga_digimon($digimones[1], $coleccion[0]);
$d2 = otorga_digimon($digimones[2], $coleccion[0]);

$coleccion[0][$d0->get_nombre()] = $d0;
$coleccion[1][$d1->get_nombre()] = $d1;
$coleccion[2][$d2->get_nombre()] = $d2;
 
guardar($coleccion, 'data/usuarios/jugador1/coleccion');
 */
/*
$digimones23 = cargar('text/digimones');

unset($digimones23[2]['josepomon']);

guardar($digimones23, 'text/digimones');
 */
